#!/usr/bin/python
# -*- coding: utf-8 -*-

import Tkinter as tk
import mainApplication
from hillcipher.util import *

class CipherWindow(tk.Frame):
	def __init__(self):
		self.master = tk.Tk()
		self.master.columnconfigure(0, weight=1)
		self.master.rowconfigure(0, weight=1)
		self.master.title("Chiffrer")
		self.master.geometry("400x300")
		tk.Frame.__init__(self, pady=15, padx=10)
		self.message = tk.StringVar()
		self.ciphertext = tk.StringVar()
		self.grid(sticky="nesw")
		self.rowconfigure(0, weight=1)
		self.columnconfigure(0, weight=1)
		self.create_widgets()

	def create_widgets(self):
		self.backButton = tk.Button(self, text="Retour au menu", command=self.back)
		self.backButton.grid(row=0, sticky="we")

		self.keyFrame = keyFrame = tk.LabelFrame(self, text="Clé", pady=5)
		self.keyFrame.rowconfigure(0, weight=1)
		self.keyFrame.columnconfigure(0, weight=1)
		self.keyFrame.grid(row=1, sticky="swe")

		self.keyInput1 = tk.Spinbox(keyFrame, from_=0, to=25)
		self.keyInput1.grid(row=0, column=0, padx=10, sticky="w")

		self.keyInput2 = tk.Spinbox(keyFrame, from_=0, to=25)
		self.keyInput2.grid(row=0, column=1, padx=10, sticky="e")

		self.keyInput3 = tk.Spinbox(keyFrame, from_=0, to=25)
		self.keyInput3.grid(row=1, column=0, padx=10, sticky="w")

		self.keyInput4 = tk.Spinbox(keyFrame, from_=0, to=25)
		self.keyInput4.grid(row=1, column=1, padx=10, sticky="e")

		self.messageLabel = tk.Label(self, textvariable=self.message)
		self.messageLabel.grid(row=2, pady=2, sticky="we")

		self.plaintextLabel = tk.Label(self, text="Texte en clair")
		self.plaintextLabel.grid(row=3)

		self.plaintextInput = tk.Entry(self)
		self.plaintextInput.grid(row=4, sticky="nsew")

		self.cipherButton = tk.Button(self, text="CHIFFRER", command=self.cipherHandler)
		self.cipherButton.grid(row=5, pady=10, sticky="nsew")

		self.ciphertextLabel = tk.Label(self, text="Texte chiffré")
		self.ciphertextLabel.grid(row=6)

		self.ciphertextInput = tk.Entry(self, textvariable=self.ciphertext)
		self.ciphertextInput.grid(row=7, sticky="nsew")

	def back(self):
		self.master.destroy()
		menuWindow = mainApplication.MainApplication()
		menuWindow.master.mainloop()

	def cipherHandler(self):
		a = int(self.keyInput1.get())
		b = int(self.keyInput2.get())
		c = int(self.keyInput3.get())
		d = int(self.keyInput4.get())
		key = [
			[a, b],
			[c, d]
		]

		if not inversible(key):
			self.message.set("Clé non inversible")
			return

		cipher = CrypterHill(self.purgeInput(self.plaintextInput.get()), key)

		self.ciphertext.set(cipher)

	def purgeInput(self, plaintext):
		charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		cleanPlaintext = ""
		for c in plaintext:
			if c in charset:
				cleanPlaintext += c
		return cleanPlaintext