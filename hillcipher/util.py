from numpy import *

# Renvoie le PGCD des deux nombres passe en parametre
def pgcd(a,b):
    if b==0:
        return a
    else:
        r=a%b
        return pgcd(b,r)

# Renvoie true si la matrice passee en parametre est inversible modulo 26
# On cherche d'abord la valeur du determinant de la matrice puis on teste si le determinant est premier avec 26
def inversible(matrix):
    det = int(round(linalg.det(matrix)))
    return pgcd(det, 26) == 1

# Renvoie le nombre de cles possibles pour une cle de taille n
def keys_num(n):
    return 26**(n**2)*(euler(26) / 26.0)

# Transpose la chaine de caractere en liste d'entiers associes
def transposerIndices(message):
    message = message.upper()
    digits = []
    for c in message:
        num = ord(c) - ord('A') # Lettre vers Indice
        if num not in range(26): # Permet de savoir si le caractere est valide (entre A et Z)
            continue # Permet d'aller a la prochaine iteration sans executer les instructions
        digits.append(num)
    return digits

# Transpose la liste d'entiers passee en parametre en sa chaine associee
def transposerLettre(digits):
    message = ""
    for c in digits:
        char = chr(int(c) + ord('A')) # Indice vers lettre
        message += char
    return message

# Permet de grouper une liste en groupes de n, si il n'y a pas assez d'elements pour faire un groupe, du bourrage est ajoute
def grouper(digits, n):
    d = [digits[i:i+n] for i in range(0, len(digits), n)]
    if len(d[-1]) == 1:
         d[-1].append(25)
    return d

# Renvoie l'inverse du nombre passe en parametre modulo 26 par la methode d'euler
def modInv(a):
    return (a**11)%26

# On passe en parametre le texte a chiffrer et la cle de chiffrement a utiliser puis on renvoie le ciphertext
def CrypterHill(plaintext, cle):
    # On verifie que la cle entree en parametre est bien une matrice carree
    if shape(cle)[0] != shape(cle)[1]:
        return

    k = shape(cle)[0]

    # On verifie que la taille de la cle est bien superieure a la taille du message a chiffrer
    if k > len(plaintext):
        return

    cipherText=[]
    paquets = grouper(transposerIndices(plaintext), k) # On groupe les indices associes
    for x in paquets:
        y = dot(cle, x) # On effectue le produit matriciel entre la cle et le groupe d'indices
        for c in y:
            cipherText.append(c % 26)
    return transposerLettre(cipherText) # On transpose la liste d'indices obtenue en une chaine de caractere

# On passe en parametre le texte a dechiffrer et la cle de chiffrement a utiliser puis on renvoie le plaitext
def DCrypteHill(cipherText, cle):
    # On verifie que la cle entree en parametre est bien une matrice carree
    if shape(cle)[0] != shape(cle)[1]:
        return

    k = shape(cle)[0]

    # On verifie que la taille de la cle est bien superieure a la taille du message chiffrer
    if k > len(cipherText):
        return
    plaintext = []
    groupeIndicesCode = grouper(transposerIndices(cipherText), k) # On groupe les indices associes
    inverseCle = inverseMatrice(cle) # On inverse la cle pour pouvoir dechiffrer le message

    for groupe in groupeIndicesCode:
        resultat = dot(inverseCle, groupe)  # On effectue le produit matriciel entre la l'inverse de la cle et le groupe d'indices
        for indice in resultat:
            plaintext.append(round(indice) % 26)

    return transposerLettre(plaintext) # On transpose la liste d'indices obtenue en une chaine de caractere


# Renvoie l'inverse de la matrice passee en parametre
def inverseMatrice(matriceAInverser):
    return dot(inverseDeterminant(matriceAInverser), matriceCofacteurs(matriceAInverser)) % 26


# Renvoie l'inverse du determinant de la matrice passee en parametre par l'algorithme d'euclide
def inverseDeterminant(matrice):
    determinant = int(round(linalg.det(matrice)))
    for i in range(1, 26):
        if (i * determinant) % 26 == 1:
            return i
    raise ValueError("error no inverse mod")

# La matrice des cofacteurs pour une matrice 2, 2 ou 3, 3
def matriceCofacteurs(matrice):
    r = shape(matrice)[0] # taille de la matrice

    if r not in range(2, 4):
        return

    if r == 2:
        a = matrice[0][0]
        b = matrice[0][1]
        c = matrice[1][0]
        d = matrice[1][1]
        m = [[d, -b],[-c, a]]

    elif r == 3:
        a = matrice[0][0]
        b = matrice[0][1]
        c = matrice[0][2]
        d = matrice[1][0]
        e = matrice[1][1]
        f = matrice[1][2]
        g = matrice[2][0]
        h = matrice[2][1]
        i = matrice[2][2]

        m = transpose([
            [linalg.det([[e, f], [h, i]]) % 26, -1 * linalg.det([[d, f], [g, i]]) % 26, linalg.det([[d, e], [g, h]]) % 26],
            [-1 * linalg.det([[b, c], [h, i]]) % 26, linalg.det([[a, c], [g, i]]) % 26, -1 * linalg.det([[a, b], [g, h]]) % 26],
            [linalg.det([[b, c], [e, f]]) % 26, -1 * linalg.det([[a, c], [d, f]]) % 26, linalg.det([[a, b], [d, e]]) % 26]
        ])

    return m